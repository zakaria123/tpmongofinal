package tpFinal.repository;

import java.util.List;

import org.mongodb.morphia.Datastore;

import tpFinal.model.AdwordsAccount;
import tpFinal.model.AdwordsCampaign;
import tpFinal.model.BingAccount;
import tpFinal.model.BingCampaign;
import tpFinal.model.Organisation;

public class ClientSetupRepository extends BasicRepository {

	public ClientSetupRepository(Datastore ds) {
		super(ds);
	}
	
	public void deleteAll() {
		List<Organisation> organisations = findAllByClass(Organisation.class);
		organisations.forEach(organisation ->{
			organisation.getAccounts().forEach(account ->{
				account.setOrganisation(null);
				save(account);
			});
			organisation.getOrders().forEach(order ->{
				order.getCampaigns().forEach(campaign ->{
					campaign.setOrder(null);
					save(campaign);
				});
			});
		});
		deleteAllByClass(Organisation.class);
	}
	
	public AdwordsCampaign fetchFreeAdwordsCampaignFromAccount(String accountID) {
		return ds.find(AdwordsCampaign.class)
				.field("order").doesNotExist()
				.field("account_id").equal(accountID).get();
	}
	
	public BingCampaign fetchFreeBingCampaignFromAccount(String accountID) {
		return ds.find(BingCampaign.class)
				.field("order").doesNotExist()
				.field("account_id").equal(accountID).get();
	}
	
	public AdwordsAccount fetchFreeAdwordsAccount() {
		return ds.find(AdwordsAccount.class)
				.field("organisationID").doesNotExist().get();
	}
	
	public BingAccount fetchFreeBingAccount() {
		return ds.find(BingAccount.class)
				.field("organisationID").doesNotExist().get();
	}
	
}
