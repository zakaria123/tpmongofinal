package tpFinal.repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.Datastore;

import tpFinal.model.AdwordsAccount;
import tpFinal.model.AdwordsCampaign;
import tpFinal.model.AdwordsCampaignStat;
import tpFinal.model.BingAccount;
import tpFinal.model.BingCampaign;
import tpFinal.model.BingCampaignStat;
import tpFinal.model.Campaign;
import tpFinal.model.CampaignStat;
import tpFinal.utils.DateUtils;

public class AdEngineRepository extends BasicRepository {

	public AdEngineRepository(Datastore ds){
		super(ds);
	}
	
	public void deleteAll() {
		deleteAllByClass(BingCampaignStat.class);
        deleteAllByClass(BingCampaign.class);
        deleteAllByClass(BingAccount.class);

        deleteAllByClass(AdwordsCampaignStat.class);
        deleteAllByClass(AdwordsCampaign.class);
        deleteAllByClass(AdwordsAccount.class);
	}
	
	public void generateStats(Date start, Date end){
		generateCampaignStatsByCampaignBetweenDates(AdwordsCampaign.class, start, end);
		generateCampaignStatsByCampaignBetweenDates(BingCampaign.class, start, end);
	}
	
	private <T> void generateCampaignStatsByCampaignBetweenDates(Class<T> campaignClass, Date start, Date end){
		List<T> campaigns= findAllByClass(campaignClass);
		campaigns.forEach(b->{
			Campaign campaign = (Campaign) b;
			generateRandomizedCampaignStatsBetweenDates(campaignClass, campaign.getId(), start, end);
		});
	}
	
	private <T> void generateRandomizedCampaignStatsBetweenDates(Class<T> campaignClass, ObjectId campaignId, Date start, Date end) {
		final LocalDate MAX_DATE = DateUtils.fromDate(end);
		String objectClassName = (campaignClass.getSimpleName().equals(BingCampaign.getClassName()))? BingCampaignStat.getClassName() : AdwordsCampaignStat.getClassName();
		for(LocalDate current= DateUtils.fromDate(start) ; current.isBefore(MAX_DATE) ; current = current.plusDays(1)) {
			Date date = DateUtils.parseLocalDateToDate(current);
			CampaignStat campaignStat = CampaignStat.createStat(objectClassName, campaignId, 40, 10, 0.5, date);
			save(campaignStat);
		}
	}
	
}
