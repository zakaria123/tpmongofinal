package tpFinal.repository;

import static tpFinal.utils.DateUtils.createCorrectlySettedDate;

import java.time.LocalDate;
import java.util.Date;

import org.mongodb.morphia.Datastore;

import tpFinal.model.AdwordsCampaign;
import tpFinal.model.AdwordsCampaignStat;
import tpFinal.model.BingCampaign;
import tpFinal.model.BingCampaignStat;
import tpFinal.model.CampaignStatId;
import tpFinal.model.Order;
import tpFinal.utils.DateUtils;

public class BudgetRepository extends BasicRepository {
	public BudgetRepository(Datastore ds){
		super(ds);
	}
	
	public double countAvailableConversionsAtDate(Order order, Date date) {
		return getConversionsDisponible(order, date);
	}
	
	public double getCostConversionsAtDate(Order order, Date date) {
		return getCostConversions(order, date);
	}
	
	public double calculateAvailableBudget(Order order) {
		return calculBudgetDisponible(order);
	}
	
	protected double calculBudgetDisponible(Order order) {
		Date start = order.getStart();
		Date end = order.getEnd();
		
		Date today = createCorrectlySettedDate(new Date());
		final LocalDate MAX_DATE = DateUtils.fromDate(today);
		final LocalDate MAX_DATE_2 = DateUtils.fromDate(end);
		
		double depenseUntilNow = 0;
		
		for(BingCampaign campaign : order.getBingCampaigns()){
			for(LocalDate current= DateUtils.fromDate(start) ; current.isBefore(MAX_DATE) && current.isBefore(MAX_DATE_2) ; current = current.plusDays(1)) {
				Date date = DateUtils.parseLocalDateToDate(current);
				BingCampaignStat stat = ds.find(BingCampaignStat.class)
						.field("_id").equal(new CampaignStatId(date, campaign.getId())).get();
				if(stat != null)
					depenseUntilNow += stat.getCost();
			}
		}
		for(AdwordsCampaign campaign : order.getAdwordsCampaigns()){
			for(LocalDate current= DateUtils.fromDate(start) ; current.isBefore(MAX_DATE) && current.isBefore(MAX_DATE_2) ; current = current.plusDays(1)) {
				Date date = DateUtils.parseLocalDateToDate(current);
				AdwordsCampaignStat stat = ds.find(AdwordsCampaignStat.class)
						.field("_id").equal(new CampaignStatId(date, campaign.getId())).get();
				if(stat != null)
					depenseUntilNow += stat.getCost();
			}
		}
		double budgetDisponible = (order.getBudget() - depenseUntilNow) / (DateUtils.daysBetweenStartAndEnd(end, today) +1);
		
		return parseDouble(budgetDisponible);
	}
	
	protected double getConversionsDisponible(Order order, Date start) {
		LocalDate tmp = DateUtils.fromDate(start);
		final LocalDate first = tmp.minusDays(29);
		final LocalDate end = tmp.minusDays(1);
		
		double conversionsDisponibles = 0;
		
		for(BingCampaign campaign : order.getBingCampaigns()){
			for(LocalDate current= first ; current.isBefore(end) ; current = current.plusDays(1)) {
				Date date = DateUtils.parseLocalDateToDate(current);
				BingCampaignStat stat = ds.find(BingCampaignStat.class)
						.field("_id").equal(new CampaignStatId(date, campaign.getId())).get();
				if(stat != null)
					conversionsDisponibles += stat.getAvailableConversions();
			}
		}
		for(AdwordsCampaign campaign : order.getAdwordsCampaigns()){
			for(LocalDate current = first ; current.isBefore(end) ; current = current.plusDays(1)) {
				Date date = DateUtils.parseLocalDateToDate(current);
				AdwordsCampaignStat stat = ds.find(AdwordsCampaignStat.class)
						.field("_id").equal(new CampaignStatId(date, campaign.getId())).get();
				if(stat != null)
					conversionsDisponibles += stat.getAvailableConversions();
			}
		}
		
		return parseDouble(conversionsDisponibles / DateUtils.daysBetweenStartAndEnd(end, first));
		
	}
	
	protected double getCostConversions(Order order, Date start) {
		
		double costConversion = 0;
		double nbConversion = 0;
		LocalDate tmp = DateUtils.fromDate(start);
		final LocalDate first = tmp.minusDays(29);
		final LocalDate end = tmp.minusDays(1);
		
		for(BingCampaign campaign : order.getBingCampaigns()){
			for(LocalDate current= first ; current.isBefore(end) ; current = current.plusDays(1)) {
				Date date = DateUtils.parseLocalDateToDate(current);
				BingCampaignStat stat = ds.find(BingCampaignStat.class)
						.field("_id").equal(new CampaignStatId(date, campaign.getId())).get();
				if(stat != null) {
					costConversion += stat.getCost();
					nbConversion += stat.getConversions();
				}
			}
		}
		for(AdwordsCampaign campaign : order.getAdwordsCampaigns()){
			for(LocalDate current = first ; current.isBefore(end) ; current = current.plusDays(1)) {
				Date date = DateUtils.parseLocalDateToDate(current);
				AdwordsCampaignStat stat = ds.find(AdwordsCampaignStat.class)
						.field("_id").equal(new CampaignStatId(date, campaign.getId())).get();
				if(stat != null) {
					costConversion += stat.getCost();
					nbConversion += stat.getConversions();
				}
			}
		}
		return parseDouble(costConversion/nbConversion);
	}
		
	protected double parseDouble(double d) {
		d *= 100;
		double tmp = ((int) d);
		return tmp/100;
	}
	
}
