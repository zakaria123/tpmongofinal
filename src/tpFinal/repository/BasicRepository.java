package tpFinal.repository;

import java.util.List;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.query.Query;

public abstract class BasicRepository {

	protected Datastore ds;
	
	public BasicRepository(Datastore ds) {
		this.ds = ds;
	}
	
	public <T> Key<T> save(T object){
		return ds.save(object);
	}
	
	protected <T> Query<T> queryByClass(Class<T> classe){
		return ds.find(classe);
	}
	
	public <T> List<T> findAllByClass(Class<T> classe){
		return queryByClass(classe).asList();
	}
	
	public <T> void deleteAllByClass(Class<T> classe) {
		ds.delete(queryByClass(classe));
	}
	
}
