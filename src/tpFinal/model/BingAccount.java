package tpFinal.model;

import org.mongodb.morphia.annotations.Entity;

@Entity("BingAccount")

public class BingAccount extends Account{
    public BingAccount(){super();}
    public BingAccount(String country, String currency, String id) {
        super(country, currency, id);
    }
    
    public static String getClassName() {
		return BingAccount.class.getSimpleName();
	}
}
