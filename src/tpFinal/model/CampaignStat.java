package tpFinal.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Id;

@Embedded
public abstract class CampaignStat {
	
	@Id
	protected CampaignStatId campaignStatId;
	
	//nb de fois afficher
	protected int impressions;
	
	//nb de fois cliquer
	protected int clicks;
	
	//click et rempli formulaire
	//conversions < clicks
	protected int conversions;
	protected double cost;
	//nb d'impressions / impressions max (**nb al�atoire)
	protected double impression_share;
	
	public CampaignStat() {}

	public static CampaignStat createStat(String className, ObjectId campaignId,  int impressions, double cost, double impression_share, Date date){
		if(className.equals(BingCampaignStat.getClassName())){
			return new BingCampaignStat(campaignId, date, impressions, cost, impression_share);
		}else if(className.equals(AdwordsCampaignStat.getClassName())){
			return new AdwordsCampaignStat(campaignId, date, impressions, cost, impression_share);
		}
		throw new RuntimeException("erreur");
	}
	
	public CampaignStat(ObjectId campaign_id, Date date, int impressions, double cost, double impression_share) {
		super();
		this.campaignStatId = new CampaignStatId(date, campaign_id);
		this.impressions = impressions;
		this.clicks = (int) Math.round( Math.random() * impressions);
		this.conversions = (int) Math.round( Math.random() * clicks);
		this.cost = cost;
		this.impression_share = impression_share;
	}

	public int getImpressions() {
		return impressions;
	}

	public CampaignStatId getCampaignStatId() {
		return campaignStatId;
	}

	public void setCampaignStatId(CampaignStatId campaignStatId) {
		this.campaignStatId = campaignStatId;
	}

	public void setImpressions(int impressions) {
		this.impressions = impressions;
	}

	public int getClicks() {
		return clicks;
	}

	public void setClicks(int clicks) {
		this.clicks = clicks;
	}

	public int getConversions() {
		return conversions;
	}

	public void setConversions(int conversions) {
		this.conversions = conversions;
	}

	public double getCost() {
		return cost;
	}

	public void setCost(double cost) {
		this.cost = cost;
	}

	public double getImpression_share() {
		return impression_share;
	}

	public void setImpression_share(double impression_share) {
		this.impression_share = impression_share;
	}

	public double getCostPerConversion() {
		return cost / conversions;
	}
	
	public double getAvailableImpressions() {
		return impressions / impression_share;
	}
	
	public double getAvailableConversions() {
		return conversions / impression_share;
	}
	
}
