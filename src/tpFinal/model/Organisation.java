package tpFinal.model;

import java.util.ArrayList;
import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;

@Entity("Organisation")
public class Organisation {
    @Id
    private ObjectId id;
    
    private String name;

    private List<Order> orders;
    
    private List<AdwordsAccount> adwordsAccounts;
    private List<BingAccount> bingAccounts;

    public void addOrder(Order order){
        orders.add(order);
    }
    public void addAccount(Account account){
    	if(account instanceof BingAccount)
    		bingAccounts.add((BingAccount) account);
    	else
    		adwordsAccounts.add((AdwordsAccount) account);
    }


    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public void initArrays(){
    	orders=new ArrayList<>();
        adwordsAccounts=new ArrayList<>();
        bingAccounts=new ArrayList<>();
    }
    
    public Organisation(){
        initArrays();
    }
    public Organisation(String name) {
        this.name = name;
        initArrays();
    }

    public ObjectId getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
	public List<Account> getAccounts() {
		List<Account> accounts = new ArrayList<>();
		accounts.addAll(adwordsAccounts);
		accounts.addAll(bingAccounts);
		return accounts;
	}
	
	public void setId(ObjectId id) {
		this.id = id;
	}
	
	public List<AdwordsAccount> getAdwordsAccounts() {
		return adwordsAccounts;
	}
	public List<BingAccount> getBingAccounts() {
		return bingAccounts;
	}
    
	
    
}
