package tpFinal.model;

import org.mongodb.morphia.annotations.Entity;

@Entity("AdwordsAccount")
public class AdwordsAccount extends Account {
    public AdwordsAccount(){super();}
    public AdwordsAccount(String country, String currency, String id) {
        super(country, currency, id);
    }
    
    public static String getClassName() {
		return AdwordsAccount.class.getSimpleName();
	}
}
