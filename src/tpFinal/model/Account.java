package tpFinal.model;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Id;

@Embedded
public abstract class Account {
    protected String country;
    protected String currency;
    @Id
    protected String id;

    protected ObjectId organisationID;

    public Account(){}

    public static Account createAccount(String className,String country,String currency,String id){
        if(className.equals(AdwordsAccount.class.getSimpleName())){
            return new AdwordsAccount(country,currency,id);
        }else if(className.equals(BingAccount.class.getSimpleName())){
            return new BingAccount(country,currency,id);
        }else{
            throw new RuntimeException("erreur, vous avez pas le bon type de classe");
        }
    }

    public Account(String country, String currency, String id) {
        this.country = country;
        this.currency = currency;
        this.id = id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id=id;
    }

	public ObjectId getOrganisation() {
		return organisationID;
	}

	public void setOrganisation(ObjectId organisationID) {
		this.organisationID = organisationID;
	}
    
    

}
