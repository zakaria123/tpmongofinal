package tpFinal.model;

import org.mongodb.morphia.annotations.Entity;

@Entity("BingCampaign")
public class BingCampaign extends Campaign {
	public BingCampaign() {super();}	
    public BingCampaign(String account_id, String name, String langage, String bid, double budget) {
        super(account_id, name, langage, bid, budget);
    }
    
    public static String getClassName() {
		return BingCampaign.class.getSimpleName();
	}
}
