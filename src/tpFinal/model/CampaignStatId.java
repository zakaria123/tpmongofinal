package tpFinal.model;

import java.util.Date;

import org.bson.types.ObjectId;

public class CampaignStatId {

	protected Date date;
	protected ObjectId campaign_id;
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public ObjectId getCampaign_id() {
		return campaign_id;
	}
	public void setCampaign_id(ObjectId campaign_id) {
		this.campaign_id = campaign_id;
	}
	public CampaignStatId(Date date, ObjectId campaign_id) {
		super();
		this.date = date;
		this.campaign_id = campaign_id;
	}
	
	public CampaignStatId() {
		super();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((campaign_id == null) ? 0 : campaign_id.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CampaignStatId other = (CampaignStatId) obj;
		if (campaign_id == null) {
			if (other.campaign_id != null)
				return false;
		} else if (!campaign_id.equals(other.campaign_id))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "CampaignStatId [date=" + date + ", campaign_id=" + campaign_id + "]";
	}
	
	
	
	
}
