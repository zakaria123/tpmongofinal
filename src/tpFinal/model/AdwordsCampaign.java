package tpFinal.model;

import org.mongodb.morphia.annotations.Entity;

@Entity("AdwordsCampaign")
public class AdwordsCampaign extends Campaign {
	
    private String spend_pattern;
    
    public AdwordsCampaign(String account_id, String name, String langage, String bid, double budget, String spend_pattern) {
        super(account_id, name, langage, bid, budget);
        this.spend_pattern=spend_pattern;
    }

    public AdwordsCampaign(){
        super();
    }
    public String getSpend_pattern() {
        return spend_pattern;
    }

    public void setSpend_pattern(String spend_pattern) {
        this.spend_pattern = spend_pattern;
    }
    
    public static String getClassName() {
		return AdwordsCampaign.class.getSimpleName();
	}
}
