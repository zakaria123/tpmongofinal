package tpFinal.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;

@Entity("Order")
@Embedded
public class Order {
    private String name;
    private Date start;
    private Date end;
    private double budget;
    
    private List<BingCampaign> bingCampaigns;
    private List<AdwordsCampaign> adwordsCampaigns;

    public Order(){
        this.bingCampaigns=new ArrayList<>();
        this.adwordsCampaigns=new ArrayList<>();
    }

    public Order(String name, Date start, Date end, double budget) {
        this.name = name;
        this.start = start;
        this.end = end;
        this.budget = budget;
        this.bingCampaigns=new ArrayList<>();
        this.adwordsCampaigns=new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public List<Campaign> getCampaigns() {
    	List<Campaign> campaigns=new ArrayList<>();
    	campaigns.addAll(this.bingCampaigns);
    	campaigns.addAll(this.adwordsCampaigns);
        return campaigns;
    }

    public void addCampaign(Campaign campaign) {
    	if(campaign instanceof AdwordsCampaign)
    		adwordsCampaigns.add((AdwordsCampaign) campaign);
    	else
    		bingCampaigns.add((BingCampaign) campaign);
    }

	public List<BingCampaign> getBingCampaigns() {
		return bingCampaigns;
	}

	public List<AdwordsCampaign> getAdwordsCampaigns() {
		return adwordsCampaigns;
	}
}
