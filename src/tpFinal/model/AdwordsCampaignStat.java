package tpFinal.model;


import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;

@Entity("AdwordsCampaignStat")
public class AdwordsCampaignStat extends CampaignStat {

	public AdwordsCampaignStat() {
		super();
	}
	
	public AdwordsCampaignStat(ObjectId campaign_id, Date date, int impressions, double cost, double impression_share) {
		super(campaign_id, date, impressions, cost, impression_share);
		// TODO Auto-generated constructor stub
	}

	public static String getClassName() {
		return AdwordsCampaignStat.class.getSimpleName();
	}
}
