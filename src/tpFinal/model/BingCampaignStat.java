package tpFinal.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;

@Entity("BingCampaignStat")
public class BingCampaignStat extends CampaignStat {

	public BingCampaignStat() {
		super();
	}
	
	public BingCampaignStat(ObjectId campaign_id, Date date, int impressions, double cost,
			double impression_share) {
		super(campaign_id, date, impressions, cost, impression_share);
		// TODO Auto-generated constructor stub
	}

	public static String getClassName() {
		return BingCampaignStat.class.getSimpleName();
	}
	
	
}
