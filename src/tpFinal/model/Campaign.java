package tpFinal.model;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

@Embedded
public abstract class Campaign {

    @Id
    @Property("id")
    protected ObjectId id;

    //foreign key
    protected String account_id;
    
    private Order order;
    
    private String name;
    private String langage;
    private String bid;
    private double budget;

    public Campaign(String account_id, String name, String langage, String bid, double budget) {
    	this.account_id = account_id;
        this.name = name;
        this.langage = langage;
        this.bid = bid;
        this.budget = budget;
    }

    public Campaign() {

    }
    public static Campaign createCompaign(String className,String account_id, String name, String langage, String bid, double budget, String spend_pattern){
        if(className.equals(AdwordsCampaign.class.getSimpleName())){
            return new AdwordsCampaign(account_id,name,langage,bid,budget,spend_pattern);
        }else if(className.equals(BingCampaign.class.getSimpleName())){
            return new BingCampaign(account_id,name,langage,bid,budget);
        }
        return null;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLangage() {
        return langage;
    }

    public void setLangage(String langage) {
        this.langage = langage;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }


    public Order getOrder() {
    	return order;
    }

    public void setOrder(Order order) {
    	this.order = order;
    }
    
    public void embedOrder(Order order) {
    	Order tmp = new Order();
    	tmp.setBudget(order.getBudget());
    	tmp.setEnd(order.getEnd());
    	tmp.setName(order.getName());
    	tmp.setStart(order.getStart());
    	this.order = tmp;
    }
}
