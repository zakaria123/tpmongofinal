package tpFinal.model;

import ad_engine.AdEngine;

public enum Actions {
    generate("generate"){
        @Override
        public void value(String[] args) {
            String arg = args[2].trim();
            String dateStr = args[3].trim();
            secondArgValidation(arg);
            thirdArgValidation(dateStr);
            main.generateStats(dateStr);
        }

        private void secondArgValidation(String arg) {
            if(!arg.equals("--date_start"))
                throw new RuntimeException("Va faire coiffeur");
        }
        
        private void thirdArgValidation(String arg) {
            if (!arg.matches("^[0-9]{8}$"))
                throw new RuntimeException("shiitt");
        }

    },
    delete("delete"){
        @Override
        public void value(String[] args) {
        	main.deleteAll();
        }
    },
    load("load"){
        @Override
        public void value(String[] args) {
            String arg = args[2].trim();
            String fileName=args[3].trim();
            validateFirstArgument(arg);
            main.load(fileName);
        }

        private void validateFirstArgument(String arg) {
            if(!arg.equals("--filename")) {
                throw new RuntimeException("Va faire coiffeur");
            }
        }
    };
	
    protected String action;
    protected AdEngine main;
    Actions(String action){
        this.action=action;
    };

    public void setMain(AdEngine main) {
    	this.main = main;
    }
    
    public String getAction() {
        return action;
    }

    public abstract void value(String[] args);


}
