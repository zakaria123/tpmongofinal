package tpFinal.utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class JSONUtils {

	public static JSONObject getCurrentObject(JSONArray jsonAccount, int i) throws ParseException {
		JSONParser jsonParser = new JSONParser();
		return (JSONObject) jsonParser.parse("{" + ((String) jsonAccount.get(i)).replace("'", "\"").trim() + "}");
	}
	
	public static String getValueFromJSONObject(JSONObject object, String key) {
		return object.get(key).toString();
	}
	
}
