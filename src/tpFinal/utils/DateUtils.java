package tpFinal.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class DateUtils {
	public static Date addDays(Date date, int days){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days); //minus number would decrement the days
		return cal.getTime();
	}
	
	public static LocalDate fromDate(Date date) {
		Instant instant = Instant.ofEpochMilli(date.getTime());
		return LocalDateTime.ofInstant(instant, ZoneId.systemDefault())
				.toLocalDate();
	}
	
	public static Date parseLocalDateToDate(LocalDate date) {
		return Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}
	
	public static Date createCorrectlySettedDate(Date date) {
		return parseLocalDateToDate(fromDate(date));
	}
	
	public static Date getYesterday() {
		LocalDate date = fromDate(new Date());
		date.minusDays(1);
		return parseLocalDateToDate(date);
	}
	
	public static int daysBetweenStartAndEnd(Date end, Date start) {
		long diff = end.getTime() - start.getTime();
	    return (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

	}
	
	public static int daysBetweenStartAndEnd(LocalDate end, LocalDate start) {
		long diff = parseLocalDateToDate(end).getTime() - parseLocalDateToDate(start).getTime();
	    return (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

	}
}
