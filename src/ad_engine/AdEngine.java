package ad_engine;

import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;

import tpFinal.model.Account;
import tpFinal.model.Actions;
import tpFinal.model.AdwordsAccount;
import tpFinal.model.AdwordsCampaign;
import tpFinal.model.BingCampaign;
import tpFinal.model.Campaign;
import tpFinal.repository.AdEngineRepository;
import tpFinal.utils.JSONUtils;

public class AdEngine {

	private AdEngineRepository repo;

	private AdEngine(AdEngineRepository repo) {
		this.repo = repo;
	}
	
	public static void main(String[] args){
		Morphia morphia = new Morphia();
		MongoClient mongoClient = new MongoClient("localhost", 27017);
        try {
        	Datastore ds = morphia.createDatastore(mongoClient, "tpfinal");
			AdEngineRepository repository = new AdEngineRepository(ds);
			AdEngine main = new AdEngine(repository);
			if(args.length>0 && args.length%2==0){
		        String actionArg = args[1].trim();
		        Actions action = Actions.valueOf(actionArg);
		        action.setMain(main);
		        action.value(args);
	        }
		} catch (Throwable ex) {
			System.err.println("Failed to create sessionFactory object." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public void generateStats(String dateStr) {
		SimpleDateFormat dtf = new SimpleDateFormat("yyyyMMdd");
		try {
			Date start = dtf.parse(dateStr);
			Date end = new Date();
			repo.generateStats(start, end);
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void deleteAll() {
		repo.deleteAll();
	}
	
	public void load(String fileName) {
		try {
			JSONParser jsonParser = new JSONParser();
			File file=new File("./"+fileName);
			Object object = jsonParser.parse(new FileReader(file));
			JSONObject jsonObject = (JSONObject) object;
			JSONArray jsonAccount=(JSONArray) jsonObject.get("Account");
			if(jsonAccount==null)
				throw new RuntimeException("yoooo, ton json est pas dans le bon format");
			loadAccountsAndCampaignsFromJSONArray(jsonAccount);
		}catch (Exception e){
			System.out.println("bugg va faire coiffeur");
			e.printStackTrace();
		}

	}

	private void loadAccountsAndCampaignsFromJSONArray(JSONArray accounts) {	
		for(int i=0; i<accounts.size(); i++) {
			try {
				JSONObject accountJson= JSONUtils.getCurrentObject(accounts, i);
				String className = JSONUtils.getValueFromJSONObject(accountJson, "className");
				String country = JSONUtils.getValueFromJSONObject(accountJson, "country");
				String currency = JSONUtils.getValueFromJSONObject(accountJson, "currency");
				String id = JSONUtils.getValueFromJSONObject(accountJson, "_id");
				Account account = Account.createAccount(className, country, currency, id);
				repo.save(account);
				
				JSONArray campaigns = (JSONArray) accountJson.get("Campaigns");
				createCampaignsFromJSONArray(campaigns, account);
				
			}catch(Exception e) {
				System.out.println(e.getMessage());
			}
		}
		
	}

	private void createCampaignsFromJSONArray(JSONArray campaigns, Account account) {
		for(int i=0; i<campaigns.size(); i++) {
			JSONObject jsonCampaign = (JSONObject) campaigns.get(i);
            String accountId = account.getId();
            String name = jsonCampaign.get("name").toString();
            String language = jsonCampaign.get("language").toString();
            String bid = jsonCampaign.get("bid").toString();
            Double budget = Double.parseDouble(jsonCampaign.get("budget").toString());
            String spendPattern = (account instanceof AdwordsAccount)? jsonCampaign.get("spend_pattern").toString() : null;
            String className = (account instanceof AdwordsAccount)? AdwordsCampaign.getClassName():BingCampaign.getClassName();
            repo.save(Campaign.createCompaign(className, accountId, name, language, bid, budget, spendPattern));
		}
	}
	
}
