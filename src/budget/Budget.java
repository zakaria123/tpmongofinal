package budget;

import java.util.Date;
import java.util.List;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;

import tpFinal.model.Organisation;
import tpFinal.repository.BudgetRepository;
import tpFinal.utils.DateUtils;

public class Budget {
	
	private BudgetRepository repo;
	
	private Budget(BudgetRepository repo) {
		this.repo = repo;
	}
	
	public static void main(String[] args) {
		Morphia morphia = new Morphia();
		MongoClient mongoClient = new MongoClient("localhost", 27017);
        try {
        	Datastore ds = morphia.createDatastore(mongoClient, "tpfinal");
        	BudgetRepository repository = new BudgetRepository(ds);
        	Budget main = new Budget(repository);
        	main.calculateAvailableBudget(DateUtils.createCorrectlySettedDate(new Date()));
			
		} catch (Throwable ex) {
			System.err.println("Failed to create sessionFactory object." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	
	public void calculateAvailableBudget(Date date) {
		List<Organisation> orgs = repo.findAllByClass(Organisation.class);
		orgs.forEach(org->{
			org.getOrders().forEach(order ->{
				System.out.println(order.getName());
				System.out.println( "BUDGET DISPO : "+repo.calculateAvailableBudget(order));
				System.out.println( "NB CONVERSIONS DISPO : "+repo.countAvailableConversionsAtDate(order, date));
				System.out.println( "COST CONVERSIONS : "+repo.getCostConversionsAtDate(order, date));
				System.out.println();
			});
		});
	}
}
