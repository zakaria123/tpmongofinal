package client_setup;

import java.io.File;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.bson.types.ObjectId;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Key;
import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;

import tpFinal.model.Account;
import tpFinal.model.AdwordsAccount;
import tpFinal.model.Campaign;
import tpFinal.model.Order;
import tpFinal.model.Organisation;
import tpFinal.repository.ClientSetupRepository;
import tpFinal.utils.DateUtils;
import tpFinal.utils.JSONUtils;

public class ClientSetup {

	private ClientSetupRepository repo;
	
	private ClientSetup(ClientSetupRepository repo) {
		this.repo = repo;
	}
	
	public static void main(String[] args) {
		Morphia morphia = new Morphia();
		MongoClient mongoClient = new MongoClient("localhost", 27017);
        try {
        	Datastore ds = morphia.createDatastore(mongoClient, "tpfinal");
        	ClientSetupRepository repository = new ClientSetupRepository(ds);
			ClientSetup main = new ClientSetup(repository);
			
			if(args.length>0 && args.length%2==0){
				if(args[0].equals("--action")) {
			        String actionArg = args[1].trim();
			        if(actionArg.equals("delete")) {
			        	main.deleteAll();
			        }else if(actionArg.equals("load")) {
			        	String arg = args[2].trim();
			            String fileName=args[3].trim();
			        	if(!arg.equals("--filename")) {
			                throw new RuntimeException("Va faire coiffeur");
			            }else {
			            	main.load(fileName);
			            }
			        }
				}
	        }
			
			//--action load --filename clientSetup.json
			//--action delete
			
		} catch (Throwable ex) {
			System.err.println("Failed to create sessionFactory object." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}
	
	public void deleteAll() {
		repo.deleteAll();
	}
	
	public void load(String fileName) {
		try {
			JSONParser jsonParser = new JSONParser();
			File file=new File("./"+fileName);
			Object object = jsonParser.parse(new FileReader(file));
			JSONObject jsonObject = (JSONObject) object;
			JSONArray jsonOrganisations = (JSONArray) jsonObject.get("Organisation");
			if(jsonOrganisations == null)
				throw new RuntimeException("yoooo, ton json est pas dans le bon format");
			loadOrganisationsAndOrdersFromJSONArray(jsonOrganisations);
		}catch (Exception e){
			System.out.println(e.getMessage());
		}

	}

	private void loadOrganisationsAndOrdersFromJSONArray(JSONArray organisations) {	
		for(int i=0; i<organisations.size(); i++) {
			try {
				JSONObject organisationJson= JSONUtils.getCurrentObject(organisations, i);
				String name=JSONUtils.getValueFromJSONObject(organisationJson,"name");
				Organisation organisation = new Organisation(name);
				
				int tmp = (int) ( Math.random() * 2 + 1);
				Account account = (tmp==1)? repo.fetchFreeAdwordsAccount():repo.fetchFreeBingAccount();
				
				if(account != null) {
					organisation.addAccount(account);
					Key<Organisation> orgKey = repo.save(organisation);
					account.setOrganisation((ObjectId)orgKey.getId());
					repo.save(account);
				}
				
				JSONArray orders = (JSONArray) organisationJson.get("Orders");
				createOrdersFromJSONArray(orders, organisation);
				
			}catch(Exception e) {
				System.out.println("ERROR : "+e.getMessage());
			}
		}
		
	}

	private void createOrdersFromJSONArray(JSONArray orders, Organisation organisation) {
		SimpleDateFormat dateFormat=new SimpleDateFormat("yyyymmdd");
		for(int i=0;i<orders.size();i++){
			try {
				JSONObject orderObject=(JSONObject) orders.get(i);
				String name=JSONUtils.getValueFromJSONObject(orderObject,"name");
				String start=JSONUtils.getValueFromJSONObject(orderObject,"startDate");
				String end=JSONUtils.getValueFromJSONObject(orderObject,"endDate");
				String budget=JSONUtils.getValueFromJSONObject(orderObject,"budget");
				Date dateStart=DateUtils.parseLocalDateToDate(DateUtils.fromDate(dateFormat.parse(start)));
				Date dateEnd=DateUtils.parseLocalDateToDate(DateUtils.fromDate(dateFormat.parse(end)));
				Order order=new Order(name, dateStart,dateEnd,Double.parseDouble(budget));

				embedOrder(organisation, order);
			}catch (Exception e){
				System.out.println(e.getMessage());
			}
		}
		
		try {
			repo.save(organisation);
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	private void embedOrder(Organisation organisation, Order order) {
		try {
			List<Account> accounts = organisation.getAccounts();
			Collections.shuffle(accounts);
			
			Account account = accounts.get(0);
			if(account != null) {
				Campaign campaign = getEmbeddableCampaign(account);
				if(campaign != null) {
					order.addCampaign(campaign);
					campaign.embedOrder(order);
					repo.save(campaign);
				}
			}
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		organisation.addOrder(order);
	}
	
	private Campaign getEmbeddableCampaign(Account account) {
		if(account instanceof AdwordsAccount)
			return repo.fetchFreeAdwordsCampaignFromAccount(account.getId());
		return repo.fetchFreeBingCampaignFromAccount(account.getId());
	}
	
}
